#!/bin/sh

#define parameters which are passed in.
PORT=$1
ID=$2

#define the template.
cat  << EOF
version: '3'
# Other docker-compose examples in /examples

services:
  spigot:
    container_name: "spigot"
    image: itzg/minecraft-server
    ports:
      - "$PORT:25565"
    volumes:
      - "./mc:/data"
    environment:
      EULA: "TRUE"
      VERSION: "latest"
      TYPE: "SPIGOT"
      MIN_MEMORY: "6G"
      MAX_MEMORY: "6G"
      GID: $ID
      UID: $ID
EOF
